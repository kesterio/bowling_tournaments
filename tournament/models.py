from django.db import models
from django.contrib.auth import models as cmodels
from django.contrib.auth.models import User
# Create your models here.

class Tournament(models.Model):
    title = models.TextField()
    created = models.DateTimeField(auto_now_add=True, null=True)
    user = models.ManyToManyField(User, related_name='tournament')
    start = models.DateTimeField(null=True)
    def __str__(self):
        return self.title

class Game(models.Model):
    status = models.TextField()
    tournament = models.ForeignKey(Tournament, related_name='game', on_delete=models.CASCADE)
    user = models.ManyToManyField(User, related_name='game')
    def __str__(self):
        return self.tournament.title




        