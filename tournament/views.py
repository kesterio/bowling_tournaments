from django.shortcuts import render
from rest_framework import viewsets
from .models import *
from .serializers import *
from rest_framework.generics import RetrieveUpdateAPIView
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from rest_framework.decorators import action, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAdminUser, IsAuthenticated

# from django.contrib.auth.models import User, auth
class TournamentView(viewsets.ModelViewSet, RetrieveUpdateAPIView):
    queryset = Tournament.objects.all()
    serializer_class = TournamentSerializer
    @action(detail=True)
    def user(self, request, *args, **kwargs):
        Tournament=self.get_object()
        user=Tournament.user.all()
        return Response(UserSerializer(user, many=True).data)

    @action(detail=True)
    def game(self, request, *args, **kwargs):
        Tournament=self.get_object()
        game=Tournament.game.all()
        return Response(GameSerializer(game, many=True).data)

@permission_classes([IsAuthenticatedOrReadOnly])
class GameView(viewsets.ModelViewSet, RetrieveUpdateAPIView):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    @action(detail=True)
    def user(self, request, *args, **kwargs):
        Game=self.get_object()
        user=Game.user.all()
        return Response(UserSerializer(user, many=True).data)

    @action(detail=True)
    def tournament(self, request, *args, **kwargs):
        Game=self.get_object()
        return Response(TournamentSerializer(Game.tournament).data)

@permission_classes([IsAdminUser])
class UserView(viewsets.ModelViewSet, RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    @action(detail=True)
    def game(self, request, *args, **kwargs):
        User=self.get_object()
        game=User.game.all()
        return Response(GameSerializer(game, many=True).data)

    @action(detail=True)
    def tournament(self, request, *args, **kwargs):
        User=self.get_object()
        tournament=User.tournament.all()
        return Response(TournamentSerializer(tournament, many=True).data)


def register(request):
    if(request.method == 'POST'):
        form = UserCreationForm(request.POST)
        if(form.is_valid()):
            form.save()         
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, passowrd=password)
            login(request, user)
            return redirect('home')
    else:        
        form = UserCreationForm()

    context = {'form': form}
    return render(request, 'registration/register.html', context)

def login(request):
    if(request.method == 'GET'):
        form = UserChangeForm(request.GET)

        return

@permission_classes([IsAuthenticated])
def home(request):
    return render(request, 'index.html', {})
