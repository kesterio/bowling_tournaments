from django.urls import path, include
from . import views
from rest_framework import routers
from django.contrib.auth.models import User


router = routers.DefaultRouter()
router.register('tournament', views.TournamentView)
router.register('game', views.GameView)
router.register('user', views.UserView)

urlpatterns = [
    path('', include(router.urls)),
    path('home/', views.home, name='home'),
    path('register/', views.register, name = 'register'),
    path('login/', views.login, name = 'login'),
]
